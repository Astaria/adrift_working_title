// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Adrift_Working_TitleTarget : TargetRules
{
	public Adrift_Working_TitleTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Adrift_Working_Title");
	}
}
