// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Adrift_Working_TitleGameMode.generated.h"

UCLASS(minimalapi)
class AAdrift_Working_TitleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAdrift_Working_TitleGameMode();
};



