// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Adrift_Working_TitleEditorTarget : TargetRules
{
	public Adrift_Working_TitleEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Adrift_Working_Title");
	}
}
