// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Adrift_Working_TitleGameMode.h"
#include "Adrift_Working_TitleCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAdrift_Working_TitleGameMode::AAdrift_Working_TitleGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
